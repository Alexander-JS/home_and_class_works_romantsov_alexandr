import render_play from './render_play.js';
// render now playing

// suppor Data obj
const _Data = {
	feed: [],
	time: {
		minutes: 0,
		seconds: 0
	}
};

const _add_to_playlist = ( song )=>{

	let added = true;
	_Data.feed.map( ( item )=>{
		if( item.id === song.id ){
			console.warn( `Song (${song.name}) already in the playlist!` );
			added = false;
		}
	});
	if( added ){
		_Data.feed.push( song );
		render();
	}
}


const delete_by_id = ( id )=>{
	_Data.feed.map( ( item, i )=>{
		if( item.id === id ){
			_Data.feed.splice( i, 1 );
		}
	});
	render();
}

const render = ()=>{
	let node = document.createElement( 'ol' );
	const target = document.getElementById( 'MusicPlayList' );
				target.innerHTML = null;

	// Lazy Raviant Cleansing _Data :D
	// _Data.clean_time() ...
	_Data.time = {
		minutes: 0,
		seconds: 0
	}

	_Data.feed.map( ( item )=>{
		calcule_time( item.time );

		let li = document.createElement( 'li' );
				li.innerHTML = `
					<div class="song__name">
				  	${item.name}        
				  </div>
				  <div class="song__creator">
				  	artist:
				    ${item.creator}
				  </div>
				  <div class="song__duration">
				  	time:
				    ${item.time[ 0 ]}:${item.time[ 1 ]}
				  </div>
					<div>
						<button class="_remove_${item.id}">remove from playlist</button>
					</div>
					<div>
						<button class="_play_${item.id}">play</button>
					</div>
				  `;

		// add event listener on remove
		let button_remove = li.querySelector( `._remove_${item.id}` );
				button_remove.addEventListener( 'click', ()=>{
					delete_by_id( item.id );
				});

		// add event listener on play
		let button_play = li.querySelector( `._play_${item.id}` );
				button_play.addEventListener( 'click', ()=>{
					render_play( item );
				});
		node.appendChild( li );

	});

	// render songs on DOM
	target.appendChild( node );

	// styiled time bar
	render_all_time();
}

const calcule_time = ( arr ) =>{
	_Data.time.minutes += arr[ 0 ];
	_Data.time.seconds += arr[ 1 ];

	// curreced time
	currect_time();
}

	// i needed recursion or cycle, so i put it in a separate function
const currect_time = () =>{
	if( _Data.time.seconds > 59 ){
		let upped = _Data.time.seconds-60;
		_Data.time.minutes++;
		_Data.time.seconds = upped;
		currect_time();
	}
}

// ...
export const render_all_time = () =>{
	currect_time();
	let show_total_time = document.getElementById( 'show_total_time' );
	let minutes = _Data.time.minutes;
	let seconds = _Data.time.seconds;
	if( _Data.time.minutes < 10 ){
		minutes = `0${_Data.time.minutes}`;
	}
	if( _Data.time.seconds < 10 ){
		seconds = `0${_Data.time.seconds}`;
	}
	show_total_time.innerHTML = `total time - <b>${minutes}:${seconds}</b>`;
}

export default _add_to_playlist;