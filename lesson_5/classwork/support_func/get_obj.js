const get_obj = ( song_id, MusicList ) =>{
    let song = {};
    MusicList.map( item => {
    item.songs.map( item_2 =>{
      if( item_2.id === song_id ){
        song = item_2;

        // added artist for func rander_play './render_play.js'
        song.creator = item.title;
        
      }
    });
  });
  return song;
}
export default get_obj;