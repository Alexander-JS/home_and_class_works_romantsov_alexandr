/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
  соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:

  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.

  И зарегистрировать каждое через addEventListener на каждом из светофоров.


  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" ( моргающий желтый ).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный ( на первый клик )
      или зеленый ( на второй клик ) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/

const _tap = ( elem )=> {
  console.log( elem );

}


let a = () =>{

const set_color_toggle = new CustomEvent( "tap", {
  detail: {
    selected: 'night',
    toggle_color: ( elem )=>{
      elem.classList.add( detail.selected );
    }
  }
});

const set_night = new CustomEvent( "setNight", {
  detail: {
    class_for_item: 'yellow',
    tapped_count: 0,
    timers: [],
    cleaned: false
  }
});



let trafficLights_arr = document.querySelectorAll( '.trafficLight' );

const Do = document.getElementById( 'Do' );
let items = [];

trafficLights_arr.forEach( item => {
  items.push( item );

  item.addEventListener( "tap", function( e ){
      if( e.detail.selected === 'green' ){
        e.detail.selected = 'red';
      } else if( e.detail.selected === 'red' ){
        e.detail.selected = 'green';
      } else if( e.detail.selected === 'night' ){
        e.detail.selected = 'red';
      }
    this.classList.remove( 'green' );
    this.classList.remove( 'red' );
    this.classList.remove( 'yellow' );
    this.classList.add( e.detail.selected );
  });

  item.addEventListener( 'setNight', ( e )=>{
    // console.log( {'press button!':true, e} );

    if( e.detail.cleaned === true ){
      e.detail.cleaned = false;
      e.detail.tapped_count = 0;

      e.detail.timers.map( ( item )=>{ clearTimeout( item ); });
      // console.log( 'cleaned' );
    }

    if( e.detail.tapped_count < trafficLights_arr.length ){

      // console.log( 'setNight!' );
      let timer = setInterval( function () {
        let toggled = item.classList.contains( 'green' ) === false && item.classList.contains( 'red' ) === false;
        // let 
        if( toggled ){
          item.classList.toggle( e.detail.class_for_item );
        } else {
          clearTimeout( timer );
        }
      }, 1000 );
      e.detail.timers.push( timer );
    } else {
      e.detail.cleaned = true;
    }
    e.detail.tapped_count++;
  });



 item.addEventListener( 'click', ()=>{
    item.dispatchEvent( set_color_toggle );
  });

 item.dispatchEvent( set_night );
});

let options = {
  once: true
}
 Do.addEventListener( 'click', ()=>{
    console.error( 'Ничего не знаю, пункт 4 выполнен :)' );
    
    let i = 3;
    setInterval(function () {
      console.log( i +'..' );
      if( i === 1 ){
        console.log( 'cookies!!!!!!!!' );
      }
      i--;
    }, 1000 )
    // setTimeout(function () {
    //   console.error( '3..' );
    // }, 0 );
    
    // setTimeout(function () {
    //   console.error( '2..' );
    // }, 1000 );

    // setTimeout(function () {
    //   console.error( 'Пончики!!' );
    // }, 2000 );

    setTimeout(function () {
      window.location.reload();
    }, 3000 );
    // items.map( item => {
    //   item.dispatchEvent( set_night );
    // });
 }, options );

}


export default a;