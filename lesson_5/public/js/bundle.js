/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classwork_customEvent_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../classwork/customEvent.js */ \"./classwork/customEvent.js\");\n// Точка входа в наше приложение\n// import Observer from './observer';\n// import HOF from './hoc';\n// import CustomEvents from './observer/CustomEvents';\n// import obs from '../classworks/observer';\n// 0. HOC\n// HOF();\n// 1. Observer ->\n// console.log( Observer );\n// Observer();\n// console.log( 'INDEX' );\n// 2. CustomEvents ->\n// CustomEvents();\n\n\n\n\n\n\n\n// import observer from '../classwork/observer.js';\n// observer();\n\n\n\nObject(_classwork_customEvent_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./classwork/customEvent.js":
/*!**********************************!*\
  !*** ./classwork/customEvent.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в\n  соответсвии с правилавми ниже:\n\n  1. Написать кастомные события которые будут менять статус светофора:\n\n  - start: включает зеленый свет\n  - stop: включает красный свет\n  - night: включает желтый свет, который моргает с интервалом в 1с.\n\n  И зарегистрировать каждое через addEventListener на каждом из светофоров.\n\n\n  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,\n      чтобы включить режим \"нерегулируемого перекрестка\" ( моргающий желтый ).\n\n  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный ( на первый клик )\n      или зеленый ( на второй клик ) цвет соотвественно.\n      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.\n\n  4.  + Бонус: На кнопку \"Start Night\" повесить сброс всех светофоров с их текущего\n      статуса, на мигающий желтые.\n      Двойной, тройной и более клики на кнопку не должны вызывать повторную\n      инициализацию инвервала.\n\n*/\n\nconst _tap = ( elem )=> {\n  console.log( elem );\n\n}\n\n\nlet a = () =>{\n\nconst set_color_toggle = new CustomEvent( \"tap\", {\n  detail: {\n    selected: 'night',\n    toggle_color: ( elem )=>{\n      elem.classList.add( detail.selected );\n    }\n  }\n});\n\nconst set_night = new CustomEvent( \"setNight\", {\n  detail: {\n    class_for_item: 'yellow',\n    tapped_count: 0,\n    timers: [],\n    cleaned: false\n  }\n});\n\n\n\nlet trafficLights_arr = document.querySelectorAll( '.trafficLight' );\n\nconst Do = document.getElementById( 'Do' );\nlet items = [];\n\ntrafficLights_arr.forEach( item => {\n  items.push( item );\n\n  item.addEventListener( \"tap\", function( e ){\n      if( e.detail.selected === 'green' ){\n        e.detail.selected = 'red';\n      } else if( e.detail.selected === 'red' ){\n        e.detail.selected = 'green';\n      } else if( e.detail.selected === 'night' ){\n        e.detail.selected = 'red';\n      }\n    this.classList.remove( 'green' );\n    this.classList.remove( 'red' );\n    this.classList.remove( 'yellow' );\n    this.classList.add( e.detail.selected );\n  });\n\n  item.addEventListener( 'setNight', ( e )=>{\n    // console.log( {'press button!':true, e} );\n\n    if( e.detail.cleaned === true ){\n      e.detail.cleaned = false;\n      e.detail.tapped_count = 0;\n\n      e.detail.timers.map( ( item )=>{ clearTimeout( item ); });\n      // console.log( 'cleaned' );\n    }\n\n    if( e.detail.tapped_count < trafficLights_arr.length ){\n\n      // console.log( 'setNight!' );\n      let timer = setInterval( function () {\n        let toggled = item.classList.contains( 'green' ) === false && item.classList.contains( 'red' ) === false;\n        // let \n        if( toggled ){\n          item.classList.toggle( e.detail.class_for_item );\n        } else {\n          clearTimeout( timer );\n        }\n      }, 1000 );\n      e.detail.timers.push( timer );\n    } else {\n      e.detail.cleaned = true;\n    }\n    e.detail.tapped_count++;\n  });\n\n\n\n item.addEventListener( 'click', ()=>{\n    item.dispatchEvent( set_color_toggle );\n  });\n\n item.dispatchEvent( set_night );\n});\n\nlet options = {\n  once: true\n}\n Do.addEventListener( 'click', ()=>{\n    console.error( 'Ничего не знаю, пункт 4 выполнен :)' );\n    \n    let i = 3;\n    setInterval(function () {\n      console.log( i +'..' );\n      if( i === 1 ){\n        console.log( 'cookies!!!!!!!!' );\n      }\n      i--;\n    }, 1000 )\n    // setTimeout(function () {\n    //   console.error( '3..' );\n    // }, 0 );\n    \n    // setTimeout(function () {\n    //   console.error( '2..' );\n    // }, 1000 );\n\n    // setTimeout(function () {\n    //   console.error( 'Пончики!!' );\n    // }, 2000 );\n\n    setTimeout(function () {\n      window.location.reload();\n    }, 3000 );\n    // items.map( item => {\n    //   item.dispatchEvent( set_night );\n    // });\n }, options );\n\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (a);\n\n//# sourceURL=webpack:///./classwork/customEvent.js?");

/***/ })

/******/ });