test('works', () => {
	let one = [1,2,3,4,5,6,7,8,9,10];
	let two = new Array(10);
  expect( one.length ).toBe( two.length );
})

test('do something', ()=> {
  let data = [1,2,3,4].reduce( (sum, item) => sum+item, 0 );
  expect( data ).toBe(10);
})
