import reducingCart from './testing-func';
import api from './testing-api-func';

const testingWrap = () => {

    const cart = [
      {
        name: 'Magic beans',
        price: 50
      },
      {
        name: 'Broken Sword of dumbass',
        price: 1
      },
      {
        name: 'Justice fork',
        price: 150
      }
    ];
    
    const cart_quantity = [
      {
        name: 'Magic beans',
        price: 50,
        quantity: 10
      },
      {
        name: 'Broken Sword of dumbass',
        price: 1,
        quantity: 1
      },
      {
        name: 'Justice fork',
        price: 150,
        quantity: 2
      }
    ];

    console.log( reducingCart( cart ) )

    if(  reducingCart( cart ) !== 201 ){
      throw new Error( 'Cart counting failed' );
    } else {
      console.log( 'Cart counting passed ✅' );
    }

    if(  reducingCart( cart_quantity ) !== 801 ){
      throw new Error( 'Cart counting failed' );
    } else {
      console.log( 'Cart counting passed ✅' );
    }
    //
    const url = 'cfEhMBpvci?indent=2';
    api.get__Users( url ).then( res => {
      // do_somesing( res );
      api.send__data( `http:www.json-generator.com/api/json/get/${url}`, res );
      console.log( res );
    });
    //...






}
export default testingWrap;