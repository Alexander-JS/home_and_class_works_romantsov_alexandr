/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../classworks/mediator.js":
/*!*********************************!*\
  !*** ../classworks/mediator.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _random_name_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./random_name.js */ \"../classworks/random_name.js\");\n/*\n  Написать медиатор для группы студентов.\n\n  Профессор отвечает только на вопросы старосты.\n\n  У Студента есть имя и группа которой он пренадлежит.\n  Он может запросить старосту задать вопрос или получить ответ.\n\n  Староста может добавлять студентов в группу и передавать вопрос профессору.\n*/\n\n\n\n\nconst Mediator = () => {\n  // console.log( 'mediator');\n\n  const _groups = {\n    '201': [],\n    '202': [],\n    '203': [],\n    '204': [],\n    '205': []\n    // ...\n  }\n\n  class Professor {\n    constructor( name = Object(_random_name_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])( true ) ) {\n      this.name = name;\n    }\n    answerTheQuestion( student, question ){\n      if( student.type !== 'monitor' ){\n        console.error( 'It\\' not your bussines' );\n      } else {\n        console.log( 'Yes, my dear?!' );\n        console.log( question );\n      }\n    }\n  }\n\n  class Student {\n    constructor( name = Object(_random_name_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])(true) ){\n      this.name = name;\n\n      this.type = 'student';\n      this.getAnswer.bind( this );\n\n    }\n    getAnswer( my_quest, monitor, professor ){\n      if( monitor.type === 'monitor' ){\n        monitor.askProfessor( my_quest, this, professor );\n      } else {\n        console.error( 'student is not monitor', monitor );\n      }\n    }\n    tipTheMonitor(){\n      // \n      // ?\n      //\n    }\n  }\n\n  // Monitor == Староста\n  class Monitor extends Student{\n    constructor( group ){\n      super();\n\n      this.name = Object(_random_name_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])( true );\n      this.group = group;\n      this.type = 'monitor';\n\n      this.addToGroup( this );\n      this.askProfessor.bind( this );\n    }\n    addToGroup( student ){\n      console.log( `added to group (${this.group})`, student );\n      _groups[ this.group ].push( student );\n    }\n    askProfessor( my_quest, student, professor ){\n      // Мини бонус, староста проверяет, от его ли группы студент, задающий вопрос\n\n      let in_group = false;\n      _groups[ this.group ].forEach( item =>{\n        if( item.name === student.name ){\n          in_group = true;\n        }\n      });\n      if( in_group === true ){\n        professor.answerTheQuestion( this, my_quest );\n      } else {\n        console.error( `Student ${student.name} is not in the group!`, `(${my_quest})` );\n      }\n    }\n  }\n\n\n  // testing\n  let student1 = new Student();\n  // console.log( 'student1', student1 );\n\n  let student2 = new Student();\n  let student3 = new Student();\n  let student4 = new Student();\n\n  let monitor = new Monitor( 201 );\n      monitor.addToGroup( student1 );\n      monitor.addToGroup( student2 );\n      monitor.addToGroup( student3 );\n      // student5.addToGroup( student4 );\n\n  let professor = new Professor();\n  student1.getAnswer( 'professor! 1', monitor, professor );\n  student2.getAnswer( 'professor! 2', student3, professor );\n  student4.getAnswer( 'professor! 3', monitor, professor );\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Mediator);\n\n\n//# sourceURL=webpack:///../classworks/mediator.js?");

/***/ }),

/***/ "../classworks/random_name.js":
/*!************************************!*\
  !*** ../classworks/random_name.js ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n// fisrt names\r\nconst f_m_names = [\"Allen\",\"Bob\",\"Carlton\",\"David\",\r\n\"Ernie\",\"Foster\",\"George\",\"Howard\",\r\n\"Ian\",\"Jeffery\",\"Kenneth\",\"Lawrence\",\r\n\"Michael\",\"Nathen\",\"Orson\",\"Peter\",\r\n\"Quinten\",\"Reginald\",\"Stephen\",\"Thomas\",\r\n\"Morris\",\"Victor\",\"Walter\",\"Xavier\",\"Charles\",\r\n\"Anthony\",\"Gordon\",\"Percy\",\"Conrad\",\r\n\"Quincey\",\"Armand\",\"Jamal\",\"Andrew\",\r\n\"Matthew\",\"Mark\",\"Gerald\"];\r\nconst f_w_names = [\"Alice\",\"Bonnie\",\"Cassie\",\"Donna\",\r\n\"Ethel\",\"Grace\",\"Heather\",\"Jan\",\"Katherine\",\"Julie\",\r\n\"Marcia\",\"Patricia\",\"Mabel\",\"Jennifer\",\"Dorthey\",\r\n\"Mary Ellen\",\"Jacki\",\"Jean\",\"Betty\",\"Diane\",\"Annette\",\r\n\"Dawn\",\"Jody\",\"Karen\",\"Mary Jane\",\"Shannon\",\"Stephanie\",\r\n\"Kathleen\",\"Emily\",\"Tiffany\",\"Angela\",\"Christine\",\"Debbie\",\r\n\"Karla\",\"Sandy\",\"Marilyn\",\"Brenda\",\"Hayley\",\"Linda\"];\r\n// second names\r\nconst s_names = [\"Adams\",\"Bowden\",\"Conway\",\"Darden\",\"Edwards\",\"Flynn\",\"Gilliam\",\"Holiday\",\"Ingram\",\"Johnson\",\"Kraemer\",\"Hunter\",\"McDonald\",\"Nichols\",\"Pierce\",\"Sawyer\",\"Saunders\",\"Schmidt\",\"Schroeder\",\"Smith\",\"Douglas\",\"Ward\",\"Watson\",\"Williams\",\"Winters\",\"Yeager\",\"Ford\",\"Forman\",\"Dixon\",\"Clark\",\"Churchill\",\"Brown\",\"Blum\",\"Anderson\",\"Black\",\"Cavenaugh\",\"Hampton\",\"Jenkins\",\"Prichard\"]\r\nconst random_elem = arr => arr[ Math.round( Math.random()*arr.length ) ];\r\nconst random_name = ( duble = false, first = true, gender = 'm' ) => {\r\n\tif( duble ){\r\n\t\t// duble name\r\n\t\tif( gender === 'm' ){\r\n\t\t\t// man\r\n\t\t\treturn `${random_elem( f_m_names )} ${ random_elem( s_names ) }`;\r\n\t\t} else {\r\n\t\t\t// women\r\n\t\t\treturn `${random_elem( f_w_names )} ${ random_elem( s_names ) }`;\r\n\t\t}\r\n\t} else {\r\n\t\tif( first ){\r\n\t\t\t// once first name\r\n\t\t\tif( gender === 'm' ){\r\n\t\t\t\t// man\r\n\t\t\t\treturn random_elem( f_m_names );\r\n\t\t\t} else {\r\n\t\t\t\t// women\r\n\t\t\t\treturn random_elem( f_w_names );\r\n\t\t\t}\r\n\t\t} else {\r\n\t\t\t// once second name\r\n\t\t\tif( gender === 'm' ){\r\n\t\t\t\t// man\r\n\t\t\t\treturn random_elem( s_m_names );\r\n\t\t\t} else {\r\n\t\t\t\t// women\r\n\t\t\t\treturn random_elem( s_w_names );\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n}\r\n/* harmony default export */ __webpack_exports__[\"default\"] = (random_name);\n\n//# sourceURL=webpack:///../classworks/random_name.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classworks_mediator_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../classworks/mediator.js */ \"../classworks/mediator.js\");\n\r\n\r\n\r\nObject(_classworks_mediator_js__WEBPACK_IMPORTED_MODULE_0__[\"default\"])();\r\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });