/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он пренадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/


import random_name from './random_name.js';

const Mediator = () => {
  // console.log( 'mediator');

  const _groups = {
    '201': [],
    '202': [],
    '203': [],
    '204': [],
    '205': []
    // ...
  }

  class Professor {
    constructor( name = random_name( true ) ) {
      this.name = name;
    }
    answerTheQuestion( student, question ){
      if( student.type !== 'monitor' ){
        console.error( 'It\' not your bussines' );
      } else {
        console.log( 'Yes, my dear?!' );
        console.log( question );
      }
    }
  }

  class Student {
    constructor( name = random_name(true) ){
      this.name = name;

      this.type = 'student';
      this.getAnswer.bind( this );

    }
    getAnswer( my_quest, monitor, professor ){
      if( monitor.type === 'monitor' ){
        monitor.askProfessor( my_quest, this, professor );
      } else {
        console.error( 'student is not monitor', monitor );
      }
    }
    tipTheMonitor(){
      // 
      // ?
      //
    }
  }

  // Monitor == Староста
  class Monitor extends Student{
    constructor( group ){
      super();

      this.name = random_name( true );
      this.group = group;
      this.type = 'monitor';

      this.addToGroup( this );
      this.askProfessor.bind( this );
    }
    addToGroup( student ){
      console.log( `added to group (${this.group})`, student );
      _groups[ this.group ].push( student );
    }
    askProfessor( my_quest, student, professor ){
      // Мини бонус, староста проверяет, от его ли группы студент, задающий вопрос

      let in_group = false;
      _groups[ this.group ].forEach( item =>{
        if( item.name === student.name ){
          in_group = true;
        }
      });
      if( in_group === true ){
        professor.answerTheQuestion( this, my_quest );
      } else {
        console.error( `Student ${student.name} is not in the group!`, `(${my_quest})` );
      }
    }
  }


  // testing
  let student1 = new Student();
  // console.log( 'student1', student1 );

  let student2 = new Student();
  let student3 = new Student();
  let student4 = new Student();

  let monitor = new Monitor( 201 );
      monitor.addToGroup( student1 );
      monitor.addToGroup( student2 );
      monitor.addToGroup( student3 );
      // student5.addToGroup( student4 );

  let professor = new Professor();
  student1.getAnswer( 'professor! 1', monitor, professor );
  student2.getAnswer( 'professor! 2', student3, professor );
  student4.getAnswer( 'professor! 3', monitor, professor );

}

export default Mediator;
