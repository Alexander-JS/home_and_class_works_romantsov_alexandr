// fisrt names
const f_m_names = ["Allen","Bob","Carlton","David",
"Ernie","Foster","George","Howard",
"Ian","Jeffery","Kenneth","Lawrence",
"Michael","Nathen","Orson","Peter",
"Quinten","Reginald","Stephen","Thomas",
"Morris","Victor","Walter","Xavier","Charles",
"Anthony","Gordon","Percy","Conrad",
"Quincey","Armand","Jamal","Andrew",
"Matthew","Mark","Gerald"];
const f_w_names = ["Alice","Bonnie","Cassie","Donna",
"Ethel","Grace","Heather","Jan","Katherine","Julie",
"Marcia","Patricia","Mabel","Jennifer","Dorthey",
"Mary Ellen","Jacki","Jean","Betty","Diane","Annette",
"Dawn","Jody","Karen","Mary Jane","Shannon","Stephanie",
"Kathleen","Emily","Tiffany","Angela","Christine","Debbie",
"Karla","Sandy","Marilyn","Brenda","Hayley","Linda"];
// second names
const s_names = ["Adams","Bowden","Conway","Darden","Edwards","Flynn","Gilliam","Holiday","Ingram","Johnson","Kraemer","Hunter","McDonald","Nichols","Pierce","Sawyer","Saunders","Schmidt","Schroeder","Smith","Douglas","Ward","Watson","Williams","Winters","Yeager","Ford","Forman","Dixon","Clark","Churchill","Brown","Blum","Anderson","Black","Cavenaugh","Hampton","Jenkins","Prichard"]
const random_elem = arr => arr[ Math.round( Math.random()*arr.length ) ];
const random_name = ( duble = false, first = true, gender = 'm' ) => {
	if( duble ){
		// duble name
		if( gender === 'm' ){
			// man
			return `${random_elem( f_m_names )} ${ random_elem( s_names ) }`;
		} else {
			// women
			return `${random_elem( f_w_names )} ${ random_elem( s_names ) }`;
		}
	} else {
		if( first ){
			// once first name
			if( gender === 'm' ){
				// man
				return random_elem( f_m_names );
			} else {
				// women
				return random_elem( f_w_names );
			}
		} else {
			// once second name
			if( gender === 'm' ){
				// man
				return random_elem( s_m_names );
			} else {
				// women
				return random_elem( s_w_names );
			}
		}
	}
}
export default random_name;