
/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

	

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/
// console.log(  )
import composition from './task1.js';

const create = teg =>{
	return document.createElement( teg );
}
const target = document.getElementById( 'target' );
const target_2 = document.getElementById( 'target_2' );

async function get_data( url ){
	await fetch( url ).then( res => {
		   	return res.json()
		  }).then( data => {
		  	let _temp = new HeadHunt( data );
		  	_temp.render();
		})
}

class HeadHunt {
	constructor( data ){
		this.data = data;
		this.hired = [];

		this.hire.bind( this );
		this.render.bind( this );
	}
	render(){
		let table = document.createElement( 'table' );

		const glob = this;
		this.data.map( function( item, i ) {
			let { age, name, type } = item;
			let tr = create( 'tr' )

			let td_name = create( 'td' )
			td_name.innerHTML = `${name} (${age})`;

			let td_type = create( 'td' )
			td_type.innerHTML = type;

			let td_bttn = create( 'td' )
			let button = create( 'button' );
			button.addEventListener( 'click', function() { glob.hire.call( glob, i ) });
			button.innerHTML = 'Нанять';

			td_bttn.appendChild( button );

			tr.appendChild( td_name );
			tr.appendChild( td_type );
			tr.appendChild( td_bttn );
			table.appendChild( tr );
		})
		target.innerHTML = null;
		target_2.innerHTML = null;
		target.appendChild( table );
		let ul = create( 'ul' );
		this.hired.map( function( item ){
			console.log( item );
			let { name, type } = item[ 1 ][ 0 ];
			let li = create( 'li' );
			li.innerHTML = `${name} [${type}] `;
			ul.appendChild( li );
		});
		target_2.appendChild( ul );
	}
	hire( i ){
		let _tmp = [];
		let selected = this.data.splice( i, 1 );
		let { name, gender, age, type } = selected[ 0 ];
		_tmp[ 0 ] = composition( name, gender, age, type );
		_tmp[ 1 ] = selected;
		this.hired.push( _tmp );
		this.render();
	}
}
export default get_data;