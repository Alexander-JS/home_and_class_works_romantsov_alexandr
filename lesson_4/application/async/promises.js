  /*
    PROMISES
    Способ организации асинзронного кода.
  */

  // PROMISES
  // Promise status : Pending | Fulfilled | Rejected

  /*
    FIRST PROMISE
    Статус промиса может изменится только 1 раз
  */
  const gender = ()=>{
    if( Math.round( Math.random()*100 ) > 50 ){
      return 'women';
    } else {
      return 'man';
    }
  }
  const gener_obj = ()=>{
    let _tmp_arr = new Array( 20 );
    for( let i = 0; i <= 20; i++ ){
      _tmp_arr[ i ] = {
        name: 'FName',
        second__name: 'SName',
        age: Math.round( Math.random()*100 ),
        gender: gender()
      }
    };
    return _tmp_arr;
  }

  let unswer;

  let TestPromise = new Promise( function( resolveFunc, rejectFunc ){
   setTimeout( () => {
    unswer = gener_obj();

    // переведёт промис в состояние fulfilled с результатом "result"
     resolveFunc( unswer );

    // переведёт промис в состояние Rejected с результатом "ERROR 404"
     // rejectFunc( 'ERROR 404' );
   }, 1000);
  })

  TestPromise.then(
  // В then можем передать две функции - для обработки упешного состояния и для обработки ошибки
    function( res ){
      console.log('Fulfilled: ', res );
      // console.log( TestPromise );
      return unswer;
    },
    function( error ){
      console.error( `ERROR - ${ error }`, TestPromise );
      console.log( TestPromise );
      // throw new Error('Rejected: ' + error);
      return unswer;
    }
  ).then( function( response ){
      response.map( item =>{
        if( item.age > 18 && item.age < 60 ){
          item.status = 'accepted';
        } else {
          item.status = 'no accepted';
        }
      });
      let x = [];
      response.forEach( ( item ) => {
        if( item.status === 'accepted' ){
          x.push( item );
        }
      });
      // console.log( x );
      return x;
      // console.log( 'response', response );
    }).then( res => {
      console.log( res );
    })
  // .then( res => {
  //   console.log('RES:', res);
  //   let iterableArray = res.map( item => {
  //     item.color = 'red' ;
  //     return item;
  //   });

  //   console.log('ITERABLE ARRAY', iterableArray);
  //   throw new Error('Rejected:');
  //   return iterableArray;
  // })
  // .then( function(response){
  //   console.log( 'third chain:', response );
  //   let x = [];
  //     response.forEach( function( item ){
  //       item.maxAge = 90;
  //       x.push(item);
  //     });
  //   return x;
  //   }
  // )
  // .catch( res => {
  //   console.error('we have a problems', res);
  // });


  /* LOAD CAT PROMISE */

  // function loadImagePromise( url ){
  //   return new Promise( (resolve, reject) => {
  //     let imageElement = new Image();
  //         imageElement.onload = function(){
  //           resolve( imageElement );
  //         };
  //         imageElement.onerror = function(){
  //           let message = 'Error on image load at url ' + url;
  //           new Error(message);

  //           let errorImg = new Image();
  //               errorImg.src = 'images/cat5.jpg';
  //             reject(
  //               RenderImage(errorImg)
  //             );
  //         };
  //         imageElement.src = url;
  //   });
  // }


  // loadImagePromise('images/cat1.jpg')
  //   .then( RenderImage );

  
  // Promise.all([
  //   loadImagePromise('images/cat2.jpg'),
  //   loadImagePromise('images/cat1.jpg'),
  //   loadImagePromise('images/cat3.jpg'),
  //   loadImagePromise('images/cat2.jpg'),
  //   loadImagePromise('images/cat5.jpg'),
  //   loadImagePromise('images/cat6.jpg'),
  //   loadImagePromise('images/cat7.jpg')
  // ])
  // .then( images => {
  //   console.log( images );
  //   images.forEach( img => RenderImage( img ) );
  // })
  // .catch( error => console.log('catch', error));


  // http://www.json-generator.com/api/json/get/cfWixBRmPS?indent=2/
  // //ASYNC PROMISE
  // let url = "http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2";

  // var myHeaders = new Headers();
  //     myHeaders.append("Content-Type", "text/plain");

  // const ConvertToJSON = ( data ) => data.json();


  // fetch( url, { method: 'POST', header: myHeaders} )
  //   .then( ConvertToJSON )
  //   .then( res => {
  //     return res.map( item => ({ name: item.name, age: item.age }));
  //   })
  //   .then( DataHandler );

  // function DataHandler( json ){
  //     console.log( 'json', json );
  //     json.map( item => {
  //       let elem = document.createElement('div');
  //           elem.innerHTML = `
  //             <div>
  //               ${item.name}, ${item.age}
  //             </div>
  //           `
  //         document.body.appendChild(elem);
  //     });
  // }

  // `/api/public/users/5/` => Test 1
  // `/api/public/users/5/friends` => Test 2
