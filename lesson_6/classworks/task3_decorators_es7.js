/*

  Задание:
    1. Используя функциональный декоратор, написать декоратор который будет показывать
       аргументы и результат выполнения функции.

    2. Написать декоратор для класса, который будет преобразовывать аргументы в число,
       если они переданы строкой, и выводить ошибку если переданая переменная не
       может быть преобразована в число
*/
import validedate_numbers from './support_funk/validedate_numbers.js';
 

const decorator = ({target, key, descriptor}) => {

  const originFn = descriptor.value;
  descriptor.value = function( ...args ){

    const valide = validedate_numbers( args );
    if( valide === true ){
      console.log( key, originFn( args[0], args[ 1 ] ) );
    } else {
      console.error( `argument is not a number!`, args );
      console.log( key, originFn( 0, 1 ) );
    }
   
  }
}

const Work1 = () => {
  console.log( 'task3' );

  class CoolMath {
    @decorator
    addNumbers( a,b ){ return a+b; }
    @decorator
    multiplyNumbers( a,b ){ return a*b}
    @decorator
    minusNumbers( a,b ){ return a-b }
  }
  let Calcul = new CoolMath();
  let x = Calcul.addNumbers( 2, 2 );
  let y = Calcul.multiplyNumbers( "10", "2" );
  let y1 = Calcul.multiplyNumbers( "10", "wacac" );
  let z = Calcul.minusNumbers( 10, 2 );

};

export default Work1;
