const BeachParty = () => {
/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.
  |                         |
  | Декоратор не запустился |
  |                         |

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/
const rand_from = arr => Math.round( Math.random()*arr.length-1 )

class Cooling_human {
  constructor( Human ) {
    this.human = Human

    this.human.coolers = [
      {name: 'cool tea', temperatureCoolRate: -5},
      {name: 'icecream', temperatureCoolRate: -15},
      {name: 'ice', temperatureCoolRate: -25},
      {name: 'go home from Odessa', temperatureCoolRate: -250}
    ];

    this.ChangeTemperature = this.ChangeTemperature.bind( this.human );
  }
  ChangeTemperature( temperature ) {
    console.log( this );
    let start_temperature = this.current_temperature;
    this.current_temperature += temperature;

    start_temperature = this.current_temperature + temperature;

    if( this.current_temperature > 30 ){
      let cooler = rand_from( this.coolers );
          this.current_temperature += this.coolers[ cooler ].temperatureCoolRate;

      console.log( `как жарко! Но мне помог ${this.coolers[ cooler ].name}` );
          this.coolers.splice( cooler, 1 );
    }

    if( this.current_temperature > this.max_temperature ){
      console.log( `${this.name} зажарился на солнце :(` );
    } else if( this.current_temperature < 0 ){
      console.log( `${this.name} превратился в льдину :(` );
    } else {
      console.log( `Всё хорошо, изменение температуры (${Math.round( start_temperature/2 ) }) не навредило ${this.name}` );
    }

  }
}

let Tom = {
  name: 'Tom',
  max_temperature: 60,
  current_temperature: 0
}

let CL__Tom = new Cooling_human( Tom );

CL__Tom.ChangeTemperature( 250 );

console.log( CL__Tom );

}
export default BeachParty;