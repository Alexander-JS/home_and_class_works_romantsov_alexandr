import Notifier from './notifier.js';

class BaseDecorator {
  constructor( clients ){
    let obs = clients.map( obs => {
      return new Notifier( obs );
    })
    this.clients = obs;
  }
  send__message( msg, baseNode ){
    this.clients.map( ( obs ) => {
      obs.send( msg, baseNode );
    });
  }
  add__notifier( notifier ){
    this.clients.push( notifier );
  }
}


export default BaseDecorator;
