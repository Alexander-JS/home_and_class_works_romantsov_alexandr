const validedate_numbers = arr => {
 arr.forEach( item =>{
 	if( isNaN( item+1 ) ){
 		console.error( `${item} is not a number!` );
 		item = 0;
 		return false;
 	}
 });
 return true;
}

export default validedate_numbers;