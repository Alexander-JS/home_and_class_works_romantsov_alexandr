class Notifier {
	constructor( notifiers ){
		// observer
		this.notifiers = notifiers;
	}
  send__message( msg, baseNode ){
  	this.notifiers.forEach( function ( item ) {
  		let target = baseNode.querySelector( `.notifier__item${item.name}` );
  		let messege_container = target.querySelector( '.notifier__messages' );

  		// we can tranform messege..
  		let extended_smg = msg;
			if( item.name === 'sms' ){
				extended_smg = `expensive sms: ${msg}`;

			} else if( item.name === 'telegram' ){
				extended_smg = `easy telegram: ${msg}`;
			
			} else if( item.name === 'gmail' ){
				extended_smg = `fast gmail: ${msg}`;
			
			} else if( item.name === 'viber' ){
				extended_smg = `just viber: ${msg}`;
			
			} else if( item.name === 'slack' ){
				extended_smg = `slack... ??? slack: ${msg}`;
			}
  		messege_container.innerHTML += `<div>${extended_smg}</div>`;
  	});
  }
} // end class Notifier
export class AllNotifier extends Notifier {
	constructor( arr, node ){
		super( arr );
		let input = document.getElementById( 'notifier__input' );
		arr.forEach( item =>{
			let smg = input.value;
			super.send__message.bind( this, smg, node, item.name );
		});
		input.value = '';
  }
} // end class AllNotifier
export default AllNotifier;