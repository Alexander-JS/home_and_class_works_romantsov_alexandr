
import Notifier from './support_funk/notifier.js';
import gener__obj from './support_funk/gener__obj.js';

const task2 = () =>{
/*
  Повторить задание с оповещаниями ( application/DecoratorExample ), с
  использованием нескольких уровней абстракций, а именно паттерны:
  Decorator, Observer, Fabric

  Задача: Написать динамичную систему оповещений, которая будет отправлять
  сообщения все подписаным на неё "Мессенджерам".
  Картинки мессенджеров есть в папке public/images

  Класс оповещения должен иметь декоратор на каждый мессенджер.

  При создании обьекта класса Application нужно передавать обьект
  в котором будут находится те "Мессенджеры" который в результате будут
  подписаны на этот блок приложения.

  Отправка сообщения по "мессенджерам" должна происходить при помощи
  паттерна Observer.

  При отправке сообщения нужно создавать обьект соответствующего класса,
  для каждого типа оповещания.

  let header = new Application( 'slack', 'viber', 'telegramm' );
  let feedback = new Application( 'skype', 'messanger', 'mail', 'telegram' );

  btn.addEventListener( 'click', () => header.sendMessage( msg ) );

  Архитектура:
  Application( messanges ) ->
    notfier = new Notifier
    renderInterface(){...}
  Notifier ->
    constructor() ->
      Fabric-> Фабрикой перебираете все типы месенджеров которые
      подписаны на эту Application;
    send() -> Отправляет сообщение всем подписчикам

*/

  class Application {
    constructor( notifier__targets ) {
      this.notifier__targets = notifier__targets;

      this.createInterface.call( this );
      this.notifier = new Notifier( this.notifier__targets, this.node );
      this.createInterface = this.createInterface.bind( this );
      this.node = null;
    }
    createInterface(){
      let root = document.getElementById( 'root' );
            
            // :D
            // отрисовывать html - так отрисовывать html
            if( root === null ){
              let div = document.createElement( 'div' );
                  div.id = 'root';
              document.body.appendChild( div );
              root = document.getElementById( 'root' );
            }
            //

      const AppNode = document.createElement( 'section' );

      AppNode.className = 'notifer_app';
      AppNode.innerHTML =
      `
        <div class="notifer_app--container">
          <header>
            <input id="notifier__input" maxlength="15" class="notifier__messanger" type="text"/>
            <button class="notifier__send">Send Message</button>
          </header>
          <div class="notifier__container">
          ${
            this.notifier__targets.map( item =>
              `
              <div class="notifier__item${item.name}">
                <header class="notifier__header">
                  <img width="25" src="${item.image}"/>
                  <span>${item.name}</span>
                </header>
                <div class="notifier__messages"></div>
              </div>
              `).join('')
          }
          </div>
        </div>
      `;
      const container = AppNode.querySelector( '.notifier__container' );
      const btn = AppNode.querySelector( '.notifier__send' );
      const input = AppNode.querySelector( '.notifier__messanger' );
      btn.addEventListener( 'click', () => {
        let value = input.value;
        this.notifier.send__message( value, AppNode );
        input.value = '';
      });

      this.node = AppNode;
      root.appendChild( AppNode );
    }
  }
  const Show = Me_messengers => {
    const Notifier__app = new Application( gener__obj( Me_messengers ) );
  }
  let messengers = [ 'viber', 'slack', 'telegram', 'sms', 'gmail' ];
  let messengers2 = [ 'telegram', 'sms', 'gmail' ];
  Show( messengers );
  Show( messengers2 );
}

export default task2;