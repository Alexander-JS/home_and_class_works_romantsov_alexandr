// import DecoratorExample from './DecoratorExample';
// import DecoratorBase from './decorator';

// import {FunctionDecorator} from './es7_functional_decorator';
import {es7Decorator} from './es7_functional_decorator';


// import do_some_thing from './some/do_some_thing.js';
// do_some_thing();

/*
  Демо декоратора
*/

// DecoratorBase();
// DecoratorExample();
// FunctionDecorator();
es7Decorator();
