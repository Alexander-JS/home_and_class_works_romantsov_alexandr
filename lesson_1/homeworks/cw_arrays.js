/*
  Задание:
  1. При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив, который содержит длину строк каждого из элементов.
  2. Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту.
      + Бонусный бал. Вывести на страничку списком
  3. Реализация функции поиска по массиву ITEA_COURSES.
      + Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск.

*/

const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];


//( 1 )
/*

ITEA_COURSES.forEach( ( item, i ) => {
	ITEA_COURSES[ i ] = item.length;
});
console.log( ITEA_COURSES );

*/
//( 2 )
/*

console.log( ITEA_COURSES.sort() );
// console.log( ITEA_COURSES );

//bonus
let __array__list = document.getElementById( '__array__list' );
ITEA_COURSES.forEach( ( item ) => {
	let li = document.createElement( 'li' );
	li.innerHTML = item;
	__array__list.appendChild( li );
});
//

*/
//( 3 )
/*

let __surch__list = document.getElementById( '__surch__list' );
let __surch__input = document.getElementById( '__surch__input' );
let __surch__bttn = document.getElementById( '__surch__bttn' );
let __run__render = document.getElementById( '__run__render' );

__run__render.addEventListener( 'click', () => {
	if( __run__render.checked ){
		__surch__input.addEventListener( 'input', run__render_func );
		run__render_func();
	} else {
		__surch__input.removeEventListener( 'input', run__render_func );
	}
});

__surch__bttn.addEventListener( 'click', () => {
	if( __surch__input.value !== '/' ){
		find_in_array( __surch__input.value );		
	} else {
		console.clear();
	}
});
let check__coincidence = ( key, item ) => {
	for( let i = 0; i <= key.length-1; i++ ){
		if( key[ i ].toLowerCase() === item[ i ].toLowerCase() ){
			// all ok :D
		} else {
			return false;
		}
	}
	return true;
}
let render__list = ( arr ) => {
	console.clear();
	for( let i = 0; i <= arr.length-1; i++ ){
		console.log( arr[ i ] );
	}
	__surch__list.innerHTML = null;
	arr.forEach( ( item ) => {
		let li = document.createElement( 'li' );
		li.innerHTML = item;
		__surch__list.appendChild( li );
	});
}
let find_in_array = ( key ) => {
	let arr = ITEA_COURSES;
	if( key === '' || key === null ){
		render__list( [] );
	} else if( key !== '' && key !== null && typeof Number( key ) === 'number' && key < arr.length ){
		render__list( [ arr[ key ] ] );
	} else if( key > arr.length-1 ){
		render__list( [] );
	} else {
		let rezult = [];
		let i = 0;
		arr.forEach( ( item ) => {
			if( check__coincidence( key, item ) ){
				rezult[ i ] = item;
				i++;
			}
		});
		render__list( rezult );
	}
}
// __surch__input.addEventListener( 'input', () => {

// });

let run__render_func = () => {
	if( __surch__input.value !== '/' ){
		find_in_array( __surch__input.value );
	} else {
		console.clear();
	}
}

*/