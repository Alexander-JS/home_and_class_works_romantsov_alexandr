/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/

let universe = {
  infinity: Infinity,
  good: ['cats', 'love', 'rock-n-roll'],
  evil: {
    bonuses: ['cookies', 'great look']
  }
};

const deep__freeze = ( obj ) => {
  for( let key in obj ){
    if( typeof obj[ key ] === 'object' ){
      deep__freeze( obj[ key ] );
      Object.freeze( obj[ key ] );
    }
  }
  Object.freeze( obj );
  return obj;
}

export let FarGalaxy = deep__freeze( universe );
    // FarGalaxy.good.push('javascript'); // error
    // FarGalaxy.something = 'Wow!'; // error
    // FarGalaxy.evil.humans = [];   // error

export default deep__freeze;
