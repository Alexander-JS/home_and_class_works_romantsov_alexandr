/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5

*/

let _data = {
  laws: [],
  budget: 1000000,
  citizens__satisfactions: 0,
}

class Government {
  constructor( Coutry__name ){
    this.name = Coutry__name;
    console.log( `New coutry: ${Coutry__name}` );
  }
  add__law( new_law ){
    _data.laws.push( new_law );
    _data.citizens__satisfactions += -10;
  }
  read__constitution(){
    _data.laws.map( function( item ) {
      console.log(`\nID: ${item.id}
        \nNAME: ${item.name}
        \nDESCRIPTION: ${item.description}
        `);
    });
  }
  read__law( id ){
      console.log(`
        ID: ${_data.laws[id].id}
        \nNAME: ${_data.laws[id].name}
        \nDESCRIPTION: ${_data.laws[id].description}
      `);
  }
  show__satisfactions(){
    console.log( `Citizens satisfactions: ${_data.citizens__satisfactions}` );
  }
  show__budget(){
    console.log( `Budget: ${_data.budget}` );
  }
  celebration(){
    _data.budget -= 50000;
    _data.citizens__satisfactions += 5;
  }
}
export default Government;