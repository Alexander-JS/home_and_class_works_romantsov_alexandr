let img__links = [ 'https://cdn-images-1.medium.com/fit/t/1600/480/1*Se0gnNo-tsQG-1jeu_4TJw.png' ];
let adv__links = [ 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ7iGgli2u-w_Kr2uXjgb7wHK-YwbMgdvM4bi5asFJhw3UqwIoo' ];
let posts_feed = document.getElementById( 'posts_feed' );
const random = ( min, max )=>{
  return Math.round(Math.random() * ( max - min ) + min );
}
class Post {
  constructor( title, image, description ){
    this.likes = 0;
    this.id = random( 1, 10000 );

    this.title = title;
    this.image = image;
    this.description = description;
    this.buy__count = 0;

    this.likePost.bind( this );
  }
  render(){
    if( document.querySelector( `.post_${this.id}` ) !== null ){
      let like = document.querySelector( `.post_${this.id}` );
      let span = like.querySelector( 'span' );
      span.innerHTML = this.likes;
    } else {
      let node = document.createElement( 'div' );
      node.classList.add( 'post' );
      node.innerHTML = `
        <div class="post__title">${this.title}</div>
        <div class="post__image">
          <img src="${this.image}"/>
        </div>
        <div class="post__description">${this.description}</div>
        <div class="post__footer">
          <button class="post__like post_${this.id}">Like! <span id="likespan__">${this.likes}</span></button>
        </div>`;
      let like_btn = node.querySelector( `.post_${this.id}` );
      like_btn.addEventListener( 'click', this.likePost.bind( this ) );
      posts_feed.appendChild( node );
   }
  }
  likePost(){
    console.log( 'like!!', this.id );
    this.likes++;
    this.render();
  }
}

class Advertisment extends Post{
  constructor( title, image, description ){
    super( title, image, description );
  }
  render(){
if( document.querySelector( `.post_${this.id}` ) !== null ){
      let like = document.querySelector( `.post_${this.id}` );
      let span = like.querySelector( 'span' );
      span.innerHTML = this.likes;
    } else {
      let node = document.createElement( 'div' );
      node.classList.add( 'post' );
      node.innerHTML = `
        <div class="post__title">${this.title}</div>
        <div class="post__image">
          <img src="${this.image}"/>
        </div>
        <div class="post__description">${this.description}</div>
        <div class="post__footer">
          <button class="post__like post_${this.id}">Like! <span id="likespan__">${this.likes}</span></button>
          <button class="post__buy post_b${this.id}">Buy!</button>
        </div>`;
      let like_btn = node.querySelector( `.post_${this.id}` );
      like_btn.addEventListener( 'click', this.likePost.bind( this ) );
      posts_feed.appendChild( node );

      let buy_btn = node.querySelector( `.post_b${this.id}` );
      buy_btn.addEventListener( 'click', this.buyItem.bind( this ) );
      posts_feed.appendChild( node );
   }
    }
  buyItem(){
    this.buy__count++;
    console.log( `Продукт ${this.title} куплен!` );
  }
}
let render__posts = ( count )=>{
  let count_rend = random( 4, 10 );
  if( count !== undefined ){
    count_rend = count;
  }
  for( let i = 0; i <= count_rend; i++ ){
    if( i%3 === 0 && i !== 0 ){
      let tmp = new Advertisment( `AD_${random( 1, 10 )}`, adv__links[ 0 ], 'AD_description' );
      tmp.render();
    } else {
      let tmp = new Post( `title_${random( 1, 10 )}`, img__links[ 0 ], 'description' );
      tmp.render();
    }
  }
}
render__posts( 25 );