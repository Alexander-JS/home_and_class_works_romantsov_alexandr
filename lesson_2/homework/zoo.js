// functions;
  let get__random = ( x ) => Math.round( Math.random() * x );
  //


// Задание - используя классы и (или) прототипы создать программу, которая будет
// распределять животных по зоопарку.


class Zoo {
    constructor( name ){
      this.name = name;
      this.AnimalCountn = 0;
      this.zones = {
        Mammals: [],
        Birds: [],
        Fishes: [],
        Reptile: [],
        Others: []    
    }
    // console.log( this.zones );

    }
    addAnimal( animalObj ){
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others

      if( animalObj.zone !== null ){
        this.zones[ animalObj.zone ].push( animalObj );
        // console.log( this.zones );
      } else {
        this.zones[ 'others' ].push( animalObj );
      }
    }
    removeAnimal( animalName ){
      // удаляет животное из зоопарка
      // поиск по имени

      let finded = [];
      for( let key in this.zones ){
        // console.log( this.zones[ key ] );
        this.zones[ key ].forEach( function ( item, i ) {
          if( item.name === animalName ){
            Kyiv_zoo.zones[ key ][ i ] = null;
  
            // вырезать из массива адеквато!!
          
          }
        });
      }
      // console.log( this.zones );
      console.log( `${animalName} has deen leaving` );

    }
    getAnimal( type, value ){
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска

      let finded = [];
      for( let key in this.zones ){
        this.zones[ key ].forEach( function ( item, i ) {
          if( value === 'name' && item.name === type ){
            finded.push( Kyiv_zoo.zones[ key ][ i ] );
          } else if( value === 'type' && item.type === type ){
            finded.push( Kyiv_zoo.zones[ key ][ i ] );
          } else if( value === 'food__type' && item.food__type === type ) {
            finded.push( Kyiv_zoo.zones[ key ][ i ] );
          }
        });
      }
      console.log( `get ${value} ( ${type} )`, finded );
    }
    countAnimals(){
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат

      let lenght = 0;
      for( let key in this.zones ){
        if( this.zones[ key ] !== null ){
          lenght += this.zones[ key ].length;
        }
      }
      console.log( `animals count - ${lenght}` );
    }
  }

class Animal {
  constructor( name, phrase, food__type ){
    this.name = name;
    this.phrase = phrase;
    this.food__type = food__type;
  }
  eatSomething(){
    if( this.food__type === 'herbivore' ){
      console.log( `${this.name} is eating aple` );
    } else if( this.food__type === 'carnivore' ){
      console.log( `${this.name} is eating meet` );
    }
  }
}
///////
class Mammals extends Animal{
  constructor( name, phrase, food__type, type, speed ){
    super( name, phrase, food__type );
    this.zone = 'Mammals';

    this.type = type;
    this.speed = speed;
    // console.log( this );
  }
  runing(){
    console.log( `${this.name} is runing with ${this.speed} km/h` );
  }
}

class Birds extends Animal{
  constructor( name, phrase, food__type, type, fly_height ){
    super( name, phrase, food__type );
    this.zone = 'Birds';

    this.type = type;
    this.fly_height = fly_height;
    console.log( this );
  }
  flying(){
    console.log( `${this.name} at a height of ${this.fly_height} meters` );
  }
}

class Fishes extends Animal{
  constructor( name, phrase, food__type, type, depth ){
    super( name, phrase, food__type );
    this.zone = 'Fishes';

    this.type = type;
    this.depth = depth;
    console.log( this );
  }
  swiming(){
    console.log( `${this.name} is swiming swims at a depth of ${this.depth} meters` );
  }
}
class Reptiles extends Animal {
  constructor( name, phrase, food__type, type, steal_time ){
    super( name, phrase, food__type );
    this.zone = 'Reptiles';

    this.type = type;
    this.steal_time = steal_time;
    console.log( this );
  }
  stealing(){
    console.log( `${this.name} is stealing with ${this.steal_time} hours` );
  }
}
///////
  // Тестирование:
    let Kyiv_zoo = new Zoo( 'Kyiv_zoo' );
    var Rex = new Mammals( 'Rex', 'woof', 'carnivore', 'wolf', get__random(1000000) );
    Kyiv_zoo.addAnimal( Rex );
      // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammals( 'Dex', 'woof', 'carnivore', 'wolf', 11 );
    Kyiv_zoo.addAnimal( Dex );
      // Добавит в your_zooName.zones.mamals еще одно новое животное.

    Kyiv_zoo.getAnimal( 'Rex', 'name' );  // -> { name:"Rex", type: 'wolf' ... }
    // Kyiv_zoo.getAnimal( 'wolf', 'type' ); // -> [{ RexObj },{ DexObj }];
    // Kyiv_zoo.getAnimal( 'carnivore', 'food__type' ); // -> [{ RexObj },{ DexObj }];

    Kyiv_zoo.countAnimals();
