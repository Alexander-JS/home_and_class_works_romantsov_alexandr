/*

  Задание:

  Написать функцию-конструктор, которая будет иметь приватные и публичные свойства.
  Публичные методы должны вызывать приватные.

  Рассмотрим на примере планеты:

    - На вход принимаются параметр Имя планеты.

    Создается новый обьект, который имеет публичные методы и свойства:
    - name (передается через конструктор)
    - population ( randomPopulation() );
    - rotatePlanet(){
      let randomNumber = Math.round(Math.random() * ( 1000 - 1 + 1 ) ) + 1;
      
      if ( (randomNumber % 2) == 0) {
        growPopulation();
      } else {
        Cataclysm();
      }
    }

    Приватное свойство:
    populationMultiplyRate - случайное число от 1 до 10;

    Приватные методы
    randomPopulation -> Возвращает случайное целое число от 1.000 до 10.000
    growPopulation() {
      функция которая берет приватное свойство populationMultiplyRate
      которое равняется случайному числу от 1 до 10 и умножает его на 100.
      Дальше, число которое вышло добавляет к популяции и выводит в консоль сообщение,
      что за один цикл прибавилось столько населения на планете .
    }
    Cataclysm(){
      Рандомим число от 1 до 10 и умножаем его на 10000;
      То число которое получили, отнимаем от популяции.
      В консоль выводим сообщение - от катаклизма погибло Х человек на планете.
    }

*/

const random = ( first, second )=>{
  let max = first;
  let min = second;
  if( second > first ){
    max = second;
    min = first;
  }
  return Math.round( Math.random() * ( max - min ) + min );
}

// break__into__bits - разбить_на_разряды
const b_i_b = ( value )=>{
  let str = String( value );

  // безбожно стырино с инета))))
  return str.replace( /(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ' );

  // wow!!!!
  // console.log( break__into__bits( Planet ) );
}

class Planet{
  constructor( name ){
    this.name = name;
    this.population = this.randomPopulation();
  }
  randomPopulation(){
    return random( 1000, 1 )*1000;
  }
  populationMultiplyRate(){
    let add_pop = random( 10, 1 )*100;
    this.population += add_pop;
    console.log( `За один цикл прибавилось ${b_i_b( add_pop )} населения на планете ${this.name}.` );
  }
  growPopulation(){
    this.populationMultiplyRate();
  }
  cataclysm(){
    let mort_pop = random( 10, 1 )*10000;
    this.population += mort_pop;
    console.log( `Oт катаклизма погибло ${mort_pop} населения на планете ${this.name}.` );
  }
  rotatePlanet(){
    let randomNumber = random( 1, 1000 );
      if ( randomNumber%2 === 0 ) {
        this.growPopulation.call( this );
      } else {
        this.cataclysm.call( this );
      }
  }
}

let planet_1 = new Planet( 'Upiter' );
planet_1.rotatePlanet();

console.log( planet_1 );
