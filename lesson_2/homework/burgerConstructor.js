let auto__save__to = ( elem, arr, show = false, ret = false )=>{
  arr[ arr.length ] = elem;
  if( show === true ){console.log( arr )}
  if( ret === true ){return elem}
}
let crash_console = ( method )=>{
  if( method === 'alert' ){
    console.log = function( item ){
      alert( item );
    }
    }else{
      console.log = function(){}
  }
}


const create__new__variable = ( name, value )=>{
  if( window[ name ] === undefined ){
    window[ name ] = value;
  } else {
    console.log( "переменная, функция или объект с таким именем уже был создан!" );
  }
}



// crash_console( '' );

  /*
    Задание:

    1. Создать конструктор бургеров на прототипах ("классы - синтаксический сахар над прототипами" - (Андрей Даценко)), которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new Burger( 'Hamburger',[ ...Массив с ингредиентами ] , 20 );

      Моделька для бургера:

      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]
------------------------------------------------------------







        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order( 'Hamburger' ); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order( '', 'has', 'Название ингредиента' ) -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', ['Названия ингредиентов', ...]) ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order( '', 'except', 'Булка' ) -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно, например, спрашивать через Confirm, подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню ( 3-4 шт );
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов
        5. MEGABONUS - Сделать графическую оболочку для программы.

  */



const random = (x)=> {let y = Math.round(Math.random()*x);if(typeof x!=='number'){return Math.round(Math.random());} else if( y === 0 ){random(x);} else {return y;}}

  const check__in__arr = ( ingr, arr )=>{
    let tmp = false;
      arr.map( function( item ){

        if( item === ingr ){
          console.log( item, ingr );
          tmp = true;
        }
      });
    return tmp;
  }

  const comp = ( arr, rnd )=>{
    let j = 0;
    arr.map( ( item, i )=>{
      if( arr[i] === rnd ){
        j = i;
      }
    });
    return j;
  }


  const re__check = ( ingredients, arr, start__name ) => {

    let input_new = false;

    if( check__in__arr( ingredients, arr ) === false ){
    for( let i = 0; true; i++ ){
        let rnd = menu_burgers[ random( menu_burgers.length ) ];
        if( confirm( `Такого бургера, к сожалению, нет.\nМожете попробовать "${rnd}"` ) ){
          return [ list_burgers__ingr[ comp( menu_burgers, rnd ) ], rnd ];
        }
      }
    } else {
      return [ ingredients, start__name ];
    }
  }


  let OurMenu = [];
  let OurOrders = [];

  const menu_burgers = [
    'Чизбургер',
    'Гамбургер',
    'БигМак',
    'БигТейсти',
    'Веджибургер',
    'Тофубургер',
    'Гарденбургер',
    'Фишбургер',
    'Чикенбургер',
    'Гамбургер с салатом',
  ];
  const list_burgers__ingr = [
    `Сыр Чеддер`,
    `Огурчик`,
    `Котлетка`,
    `Соус карри`,
    `Маслины`,
    `Кисло-сладкий соус`,
    `Кунжут`,
    `Рыбная котлета`,
    `Булка`,
    `Острый перец`
  ];

  const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
  ];

class Burger {
  constructor( name, composition, cookingTime ){
    this.name = name;
    this.composition = composition;
    this.cookingTime = cookingTime;


    this.add_to_OurOrders( this );
    this.showComposition();
  }
  add_to_OurOrders( burger ){
    OurOrders.push( burger );
    // console.clear();
    console.log( 'menu:' );
    OurOrders.map( function( item ){
      console.log( item );
    });
    console.log( '---' );
  }
  showComposition(){
    let {composition, name} = this;
    let compositionLength = composition.length;
    if( compositionLength !== 0){
      composition.map( function( item ){
        // ---------
        if( item !== composition[ 0 ] ){
          let string = `В состав бургера ${name} входит:
          `;
          string.split('');
          composition.map( function( item ){
            string += `
            `+item;
            string.split('');
          });
          console.log( string );
        }
        // ---------
      });
    }
  }
}

class Order{
  constructor( name, method, ingredients ){
    this.id = random( 10 );
    let ingredients__ = re__check( ingredients, Ingredients, name );
    // console.log( 'ingredients__', ingredients__ );
    this.name = ingredients__[ 1 ];
    let str = '';
    if( method === 'has' && ingredients__ !== '' ){
      this.orderAvailability = ingredients__[ 0 ];
      str += ` с ${ingredients__[ 0 ] }, `;
    } else if( method === 'except' ){
      this.orderException = ingredients__[ 0 ];
    }
    let all__time = 0;
    OurOrders.map( function( item, i ){
      all__time += item.cookingTime;
    });
    console.log( `Заказ ${OurOrders.length}: Бургер ${this.name} ${str} будет готов через ${all__time}` );


    this.build_burger( this.name, [str], method );
  }
  build_burger( name, ingred, method ){
    new Burger( name, ingred, 20 );
  }
}

new Order( 'Hamburger', 'has', 'Булка' );

new Order( 'Hamburger1', 'has', 'Булка' );

new Order( 'Hamburger2', 'has', 'Булка' );
